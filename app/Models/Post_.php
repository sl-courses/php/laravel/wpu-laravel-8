<?php

namespace App\Models;


class Post
{
    private static $blog_posts = [ 
        [
            "title" => "Judul Post Pertama",
            "slug" => "judul-post-pertama",
            "author" => "Dayen",
            "body" => "labore laboris Duis consectetur aute consequat aliquip voluptate eiusmod incididunt sit fugiat irure tempor mollit magna lorem aute sed labore sit lorem cupidatat aute aliqua aliqua sed occaecat sed Duis irure labore occaecat laboris dolore proident sint deserunt minim enim commodo Duis in ipsum sit tempor fugiat do ea exercitation"
        ],
        [
            "title" => "Judul Post Kedua",
            "slug" => "judul-post-kedua",
            "author" => "Abdul Kodir",
            "body" => "irure ipsum laboris sit ullamco mollit laborum voluptate voluptate nisi anim irure labore sit mollit labore dolore est quis enim est elit quis lorem do lorem esse irure qui cupidatat esse sunt veniam consequat Excepteur esse pariatur culpa qui nostrud do exercitation consequat incididunt id fugiat tempor est aliqua ad ad voluptate laborum nisi irure velit voluptate est laboris ut adipiscing enim amet reprehenderit pariatur velit laborum tempor incididunt aliquip nulla fugiat quis est voluptate anim officia labore laborum consectetur ipsum ipsum dolore Excepteur et Excepteur proident anim voluptate eu minim dolor dolore laboris amet elit sint ea exercitation minim sed tempor commodo mollit lorem cupidatat lorem laborum sint occaecat sed aute amet anim elit tempor quis tempor eu tempor elit dolor velit fugiat fugiat sunt adipiscing nulla anim voluptate do ad labore veniam irure quis quis sunt adipiscing ad sunt reprehenderit laborum magna consequat minim in reprehenderit deserunt lorem"
        ],
    ];

    public static function all() {
        return collect(self::$blog_posts);
    }

    public static function find($slug) {
        $posts = static::all();
        // $post = [];
        // foreach($posts as $p) {
        //     if($p["slug"] === $slug) {
        //         $post = $p;
        //     }
        // }
        return $posts->firstWhere('slug', $slug);
    }
}
