@extends('layouts.main')

@section('container')
    <article>
        <h2>{{ $post->title }}</h2>
        <h5>By: {{ $post->author }}</h5>
        <!-- {!! $post->body !!} ini untuk menghilangkan tag html -->
        {!! $post->body !!}
    </article>
    <a href="/posts">Back to Posts</a>
@endsection